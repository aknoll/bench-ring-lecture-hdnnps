# BENCh Ring Lecture - Hands-on Tutorial
## Predicting Vibrational Frequencies of the Formic Acid Dimer

## Usage

1. Log in to [jupyter-cloud](https://jupyter-cloud.gwdg.de) with your university
   or GWDG account credentials (Stud.IP, FlexNow, ...).
2. In the upper menu bar: tab **Git** -->*Clone a Repository**.
3. A pop up dialog opens. Insert the HTTPS clone link of this repository:
   [https://gitlab.gwdg.de/aknoll/bench-ring-lecture-hdnnps.git](https://gitlab.gwdg.de/aknoll/bench-ring-lecture-hdnnps.git)
4. Navigate to the "handson.ipynb" notebook in the appearing folder.
5. Done!

## Authors

* [Alexander Knoll](mailto:aknoll@gwdg.de) - @aknoll
